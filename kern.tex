\section{Kernels}

Efficient inner products. Fundamental insight: Optimal separating hyperplane
lives in the span of data: $\hat{w} = \sum_{i=1}^n \alpha_i y_i x_i$

``Kernel trick'': Reformulate problem s.t.\ only inner product $x_j^T x_i$ occurs. For
example the perceptron:

\vspace{-1.3em}
\begin{talign*}
    \let\displaystyle\textstyle
    \hat{w} =& \argmin_w \frac{1}{n} \sum_{i=1}^n \max(0, -y_i w^T x_i) \\
    \hat{a} =& \argmin_\alpha \frac{1}{n} \sum_{i=1}^n \max(0,-\sum_{j=1}^n
    \alpha_j y_i y_j x_j^T x_i)
\end{talign*}

For some feature transform $\phi: x \mapsto \phi(x)$, kernels solve $\phi(x)^T \phi(x')$
\green{efficiently} as $k(x, x')$. Kernelized perceptron:

\begin{enumerate}
    \item Initialize $\alpha_1 = \alpha_2 = \dots = \alpha_n = 0$
    \item For $t = 0, 1, \dots$
        \begin{enumerate}
            \item Pick $(x_i, y_i) \in D$ u.a.r.
            \item Predict $\hat{y} = \sign\left(\sum_{j=1}^n \alpha_j y_j k(x_j, x_i)\right)$
            \item If $\hat{y} \neq y_i$ set $\alpha_i \Leftarrow \alpha_i + \eta_t$
        \end{enumerate}
\end{enumerate}

$k: X \times X \Rightarrow R$ must be \emph{symmetric}: $k(x, x') = k(x', x)$

Gram matrix $K$ must be p.s.d.\ ($\forall x. x^T K x \geq 0)$ for any $n$, any
set $\{x_1, \dots, x_n\} \subseteq X$. All p.s.d.\ matrices are some kernel
and all kernels have Gram matrix.
\[
    K = \begin{bmatrix}k(x_1,x_1) & \dots & k(x_1, x_n) \\ \vdots & \ddots &
    \vdots \\ k(x_n, x_1) & \dots & k(x_n, x_n)\end{bmatrix}
%    = \begin{bmatrix}\phi(x_1)^T\phi(x_1) & \dots & \phi(x_1)^T\phi(x_n) \\ \vdots & \ddots &
%    \vdots \\ \phi(x_n)^T\phi(x_1) & \dots & \phi(x_n)^T\phi(x_n)\end{bmatrix}
\]

For $k_1, k_2$ kernel, $c > 0$ and $f$ polyn. with pos. coef. or exp.
$k_1 + k_2$, $k_1 \cdot k_2$, $c \cdot k_1$ and $f(k_1(x,x'))$
are kernels.

\begin{tabular}{ll}
    Poly. degree $= d$ & $(x^T x')^d$ \\
    Poly. degree $\leq d$ & $(x^T x' + 1)^d$ \\
    Gaussian (RBF) & $\exp(-\norm{x-x'}_2^2/h^2)$ \\
    Lapacian & $\exp(-\norm{x-x'}_1/h)$ \\
\end{tabular}

Note: $h > 0$ is bandwidth, $h \rightarrow 0$ overfits.

\textbf{k-NN}:\hspace{1em} $y = \sign\left(\sum_{i=1}^n y_i [\text{$x_i$ among k-NN of $x$}]\right)$

\green{No training}, but \red{depends on all data}.

\centerline{$y = \sign\left(\sum_{i=1}^n y_i \alpha_i k(x_i, x)\right)$}

Can use kernel as \emph{similarity function}: \green{Improved performance}, depends only on
\green{wrongly classified data}, but \red{requires training}.

\subsection{Parametric vs.\ nonparametric learning}

\emph{Parametric} have finite set of parameters (regression, perceptron), while
\emph{nonparametric} increase complexity with size of data (k-NN).

Can kernelize other tasks, such as SVM. Let $k_i =
\begin{bmatrix}y_1 k(x_1,x_i) & \dots & y_n k(x_n, x_i) \end{bmatrix}$:

\centerline{$\argmin_\alpha \frac{1}{n} \sum_{i=1}^n \max\{0, 1-y_i\alpha^Tk_i\} + \lambda \alpha^TD_y K D_y \alpha$}

\centerline{Lin.\ reg.: $\argmin_\alpha \frac{1}{n} \norm{\alpha^TK-y}_2^2 + \lambda \alpha^T K \alpha$}

Est. kernel parameters via CV. Choosing kernels requires \red{domain
knowledge}. Deal w/ overfit by regularization.
